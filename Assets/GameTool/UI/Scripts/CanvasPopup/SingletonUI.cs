using UnityEngine;

// ReSharper disable once CheckNamespace
namespace Dmobin.UIManager
{
    public class SingletonUI<T> : BaseUI where T : BaseUI
    {
        private static T singleton;

        public static T Instance
        {
            get
            {
                if (singleton == null)
                {
                    singleton = (T)FindObjectOfType(typeof(T));
                    if (singleton == null)
                    {
                        /*GameObject obj = new GameObject();
                        obj.name = "[@" + typeof(T).Name + "]";
                        singleton = obj.AddComponent<T>();*/

                        Debug.LogError(typeof(T).Name + "is Null");
                    }
                }

                return singleton;
            }
        }

        public static bool IsInstanceValid()
        {
            return singleton != null;
        }

        protected virtual void Awake()
        {
            if (singleton && singleton != this)
            {
                Destroy(gameObject);
            }
            else
            {
                singleton = (T)(BaseUI)this;
            }
        }
    }
}