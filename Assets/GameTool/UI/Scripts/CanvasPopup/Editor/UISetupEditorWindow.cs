﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;

// ReSharper disable once CheckNamespace
namespace Dmobin.UIManager.Editor
{
    public class UISetupEditorWindow : EditorWindow
    {
        private CanvasPrefTable _table;
        private SerializedObject _tableSerializedObject;
        
        [MenuItem("Dmobin/UI/CanvasManager")]
        private static void ShowWindow()
        {
            var window = GetWindow<UISetupEditorWindow>();
            window.titleContent = new GUIContent("UI Setup");
            window.Show();
        }

        private void OnEnable()
        {
            _table = Resources.Load<CanvasPrefTable>("CanvasPrefTable");
        }
        
        private string FindFilePath()
        {
            string fileName = "eUIName";

            string[] paths = AssetDatabase.FindAssets(fileName);
            
            foreach (string guid in paths)
            {
                var scriptPath = AssetDatabase.GUIDToAssetPath(guid);
                string scriptName = Path.GetFileName(scriptPath);

                if (scriptName == "eUIName.cs")
                {
                    return scriptPath.Remove(0, "Assets".Length);
                }
            }

            Debug.LogWarning("Không tìm thấy tệp " + fileName);
            return "";
        }

        private void CreateGUI()
        {
            minSize = new Vector2(800, 400);
            VisualElement root = rootVisualElement;
            
            _tableSerializedObject = new SerializedObject(_table);
            root.Add(new Button(SaveClick){text = "SAVE"});
            root.Add(new Button(() =>
            {
                Application.OpenURL(@"https://wiki.dmobin.studio/doc/canvasmanager-QghJf8ksJy");
            }){text = "DOCUMENT", style = { height = new StyleLength(20)}});
            
            SerializedProperty property = _tableSerializedObject.FindProperty("Serializers");

            var scrollView = new ScrollView(ScrollViewMode.Vertical);
            root.Add(scrollView);
            var propertyField = new PropertyField();
            propertyField.BindProperty(property);
            scrollView.Add(propertyField);
        }

        private void SaveClick()
        {
            _table.OnValidate();
                
            string path = Application.dataPath + FindFilePath();
            List<string> texts = new List<string>();
            texts.Add("namespace Dmobin.UIManager");
            texts.Add("{");
            texts.Add("    public enum eUIName");
            texts.Add("    {");
            texts.Add("        None,");
            foreach (var item in _table.Serializers)
            {
                if (item.key != "None")
                {
                    texts.Add("        " + item.key + ",");
                }
            }

            texts.Add("    }");
            texts.Add("}");
            File.WriteAllLines(path, texts.ToArray());
            AssetDatabase.ImportAsset(@"Assets" + FindFilePath());
            AssetDatabase.Refresh();
            _tableSerializedObject.ApplyModifiedProperties();
        }
    }
}