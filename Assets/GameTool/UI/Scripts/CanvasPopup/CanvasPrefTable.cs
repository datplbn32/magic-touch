﻿using System;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

// ReSharper disable once CheckNamespace
namespace Dmobin.UIManager
{
    [CreateAssetMenu(fileName = "CanvasPrefTable", menuName = "ScriptableObject/CanvasPrefTable", order = 0)]
    public class CanvasPrefTable : ScriptableObject
    {
        public List<UISerializer> Serializers = new List<UISerializer>();

#if UNITY_EDITOR
        public void OnValidate()
        {
            for (int i = 0; i < Serializers.Count; i++)
            {
                if (Serializers[i].settingUI.baseUI)
                {
                    Serializers[i].key = Serializers[i].settingUI.baseUI.name.Trim().Replace(" ","");
                }
                else
                {
                    Serializers[i].key = "";
                }
                Serializers[i].settingUI.resourcePath = GetResPath(Serializers[i].settingUI.baseUI);
            }
            EditorUtility.SetDirty(this);
        }
        
        public string GetResPath(BaseUI baseUI)
        {
            var str = AssetDatabase.GetAssetPath(baseUI);
            var index = str.LastIndexOf("Resources", StringComparison.Ordinal);
            if (index >= 0)
            {
                str = str.Substring(index);
                str = str.Remove(0, "Resources/".Length);
                
                index = str.LastIndexOf(".", StringComparison.Ordinal);
                str = str.Remove(index);

                return str;
            }

            return "";
        }
#endif
    }

    [Serializable]
    public class SettingUI
    {
        public eUIType UIType;
#if UNITY_EDITOR
        public BaseUI baseUI;
#endif
        [HideInInspector] public string resourcePath;
    }

    [Serializable]
    public class UISerializer
    {
        public string key;
        public SettingUI settingUI;
    }

    public enum eUIType
    {
        Menu,
        Popup,
        AlwaysOnTop,
    }
}