using System.Collections.Generic;
using System.Linq;
using System.Threading;
using UnityEngine;

namespace _MagicTouch.Scripts.MainScripts
{
    public struct Score
    {
        public float positionDistance;
        public float curvatureDistance;
        public float angleDistance;

        public float score
        {
            get
            {
                float posScore = Mathf.Clamp01(1f - positionDistance / 50);
                float curvScore = Mathf.Clamp01(1f - curvatureDistance / 50);
                float angleScore = Mathf.Clamp01(1f - angleDistance / 50);
                return Mathf.Clamp01((4 * posScore + 1 * curvScore + 1 * angleScore) / 6);
            }
        }

        private void InitMax()
        {
            positionDistance = curvatureDistance = angleDistance = float.MaxValue;
        }

        public static Score MaxDistance
        {
            get
            {
                var result = new Score();
                result.InitMax();
                return result;
            }
        }

        public static bool operator >(Score s1, Score s2)
        {
            return s1.score > s2.score;
        }
        public static bool operator <(Score s1, Score s2)
        {
            return s1.score < s2.score;
        }
        public static bool operator >=(Score s1, Score s2)
        {
            return s1.score >= s2.score;
        }
        public static bool operator <=(Score s1, Score s2)
        {
            return s1.score <= s2.score;
        }
    }


    /// <summary>
    /// Class to hold the recognition result, with the found gesture and score.
    /// </summary>
    public class RecognitionResult
    {
        public GesturePattern gesture;
        public Score score;
    }


    public class Recognizer : MonoBehaviour
    {
        private const int Detail = 100;


        private int _numberOfThreads = 1;

        public List<GesturePattern> patterns;


        public RecognitionResult Recognize(GestureLine lineData)
        {
            var timer = new System.Diagnostics.Stopwatch();
            timer.Start();

            var normData = NormalizeData(lineData);

            var found = findPattern(normData);

            timer.Stop();

            return found;
        }


        private GestureLine NormalizeData(GestureLine lineData)
        {
            return NormalizeDistribution(NormalizeScale(NormalizeClosedLines(lineData)), Detail);
        }

        private GestureLine NormalizeDistribution(GestureLine data, int n)
        {
            var result = new GestureLine()
            {
                ListPoints = NormalizeDistribution(data.ListPoints, n),
                ClosedLine = data.ClosedLine
            };
            return result;
        }

        private GestureLine NormalizeClosedLines(GestureLine data)
        {
            var result = new GestureLine()
            {
                ListPoints = data.ListPoints.ToList(),
                ClosedLine = data.ClosedLine
            };
            if (result.ClosedLine)
            {
                result.ListPoints.Add(Vector2.Lerp(result.ListPoints.Last(), result.ListPoints[0], 0.99f));
            }
            return result;
        }


        private List<Vector2> NormalizeDistribution(List<Vector2> path, int n)
        {

            List<float> realPos = new List<float>();

            realPos.Add(0);
            for (int i = 1; i < path.Count; i++)
            {
                var v1 = path[i - 1];
                var v2 = path[i];
                realPos.Add(realPos[i - 1] + Vector2.Distance(v1, v2));
            }

            float totalDist = realPos.Last();

            var normPos = realPos.Select(e => e / totalDist).ToList();

            var result = new List<Vector2>();

            for (int ti = 0; ti <= n; ti++)
            {
                float t = (float)ti / n;
                result.Add(FindByNormalized(path, normPos, t));
            }

            return result;
        }

        private Vector2 FindByNormalized(List<Vector2> vs, List<float> ts, float t)
        {
            for (int i = 0; i < ts.Count - 1; i++)
            {
                var t1 = ts[i];
                var t2 = ts[i + 1];
                if (t1 <= t && t <= t2)
                {
                    var v1 = vs[i];
                    var v2 = vs[i + 1];
                    float tt = Mathf.InverseLerp(t1, t2, t);
                    return Vector2.Lerp(v1, v2, tt);
                }
            }
            return t > 0.5f ? vs[vs.Count - 1] : vs[0];
        }


        private GestureLine NormalizeScale(GestureLine dataLine)
        {
            var rect = CalcRect(dataLine);
            var result = new GestureLine
            {
                ListPoints = dataLine.ListPoints.Select(e => Rect.PointToNormalized(rect, e)).ToList(),
                ClosedLine = dataLine.ClosedLine
            };
            return result;
        }


        private Rect CalcRect(GestureLine lineData)
        {
            float minx, miny, maxx, maxy;
            minx = maxx = lineData.ListPoints[0].x;
            miny = maxy = lineData.ListPoints[0].y;

            var points = lineData.ListPoints;

            for (int i = 0; i < points.Count; i++)
            {
                var p = points[i];
                minx = Mathf.Min(minx, p.x);
                maxx = Mathf.Max(maxx, p.x);
                miny = Mathf.Min(miny, p.y);
                maxy = Mathf.Max(maxy, p.y);
            }

            Rect rect = Rect.MinMaxRect(minx, miny, maxx, maxy);
            float rectsize = Mathf.Max(rect.width, rect.height);
            rect = new Rect(rect.center - new Vector2(rectsize / 2, rectsize / 2), new Vector2(rectsize, rectsize));
            return rect;
        }

        private RecognitionResult findPattern(GestureLine queryData)
        {
            var bestGesture = default(GesturePattern);
            var bestScore = Score.MaxDistance;

            var indexes = Enumerable.Range(0, 1).ToList();
            List<List<int>> permutIndexes = GenPermutations(indexes);

            var permutations = permutIndexes.Select(_ => makePermutation(queryData)).ToList();
            var singlePermutation = permutations.GetRange(0, 1);

            int n_threads = Mathf.Min(_numberOfThreads, patterns.Count);

            var threads = new List<Thread>();

            for (int threadIndex = 0; threadIndex < n_threads; threadIndex++)
            {
                int beginIndex = threadIndex * patterns.Count / n_threads;
                int endIndex = (threadIndex + 1) * patterns.Count / n_threads - 1;

                threads.Add(new Thread(() =>
                {
                    var result = SearchThroughPatterns(beginIndex, endIndex, singlePermutation);

                    lock (this)
                    {
                        if (result.score > bestScore)
                        {
                            bestScore = result.score;
                            bestGesture = result.gesture;
                        }
                    }
                }));
            }

            for (int i = 0; i < threads.Count; i++)
                threads[i].Start();

            for (int i = 0; i < threads.Count; i++)
                threads[i].Join();

            return new RecognitionResult() { gesture = bestGesture, score = bestScore };
        }

        private static List<List<int>> GenPermutations(List<int> list, int low = 0)
        {

            System.Action<int, int> swap = (a, b) => { (list[a], list[b]) = (list[b], list[a]); };

            var result = new List<List<int>>();

            if (low + 1 >= list.Count)
            {
                result.Add(new List<int>(list));
            }
            else
            {
                foreach (var p in GenPermutations(list, low + 1))
                {
                    result.Add(new List<int>(p));
                }
                for (int i = low + 1; i < list.Count; i++)
                {
                    swap(low, i);
                    foreach (var p in GenPermutations(list, low + 1))
                    {
                        result.Add(new List<int>(p));
                    }
                    swap(low, i);
                }
            }
            return result;
        }

        private GestureLine makePermutation(GestureLine data)
        {
            return data;
        }

        RecognitionResult SearchThroughPatterns(int beginIndex, int endIndex, List<GestureLine> singlePermutation)
        {
            var bestGesture = default(GesturePattern);
            var bestScore = Score.MaxDistance;

            for (int i = beginIndex; i <= endIndex; i++)
            {
                var gestureAsset = patterns[i];
                var assetData = NormalizeData(gestureAsset.GestureLine);

                //if useLinesOrder, dont calc permutations
                var permutationsToLook = singlePermutation;

                foreach (var data in permutationsToLook)
                {
                    var permutScore = CalcScore(data, assetData);

                    if (permutScore > bestScore)
                    {
                        bestScore = permutScore;
                        bestGesture = gestureAsset;
                    }
                }
            }

            return new RecognitionResult() { gesture = bestGesture, score = bestScore };
        }


        private Score CalcScore(GestureLine data1, GestureLine data2)
        {

            var lineScores = new List<Score>();

            var line1 = data1.ListPoints;

            var line2Fwd = data2.ListPoints; //forward
            var scoreFwd = CalcListScore(line1, line2Fwd, data2.ClosedLine);

            var line2Bwd = line2Fwd.AsEnumerable().Reverse().ToList(); //backwards
            var scoreBwd = CalcListScore(line1, line2Bwd, data2.ClosedLine);

            //gets best score between forward and backwards
            var score = scoreFwd > scoreBwd ? scoreFwd : scoreBwd;
            lineScores.Add(score);

            //mean score from lines
            return new Score()
            {
                positionDistance = lineScores.Select(e => e.positionDistance).Sum() / lineScores.Count,
                angleDistance = lineScores.Select(e => e.angleDistance).Sum() / lineScores.Count,
                curvatureDistance = lineScores.Select(e => e.curvatureDistance).Sum() / lineScores.Count
            };
        }


        private Score CalcListScore(List<Vector2> points1, List<Vector2> points2, bool points2IsClosed)
        {
            if (points2IsClosed)
            {
                return CalcCircularListScore(points1, points2);
            }
            return CalcLinearListScore(points1, points2);
        }


        private Score CalcCircularListScore(List<Vector2> points1, List<Vector2> points2)
        {
            List<Vector2> points2Offset = new List<Vector2>();

            Score maxScore = new Score();

            for (int offset = 0; offset < points2.Count; offset++)
            {
                points2Offset.Clear();

                for (int i = 0; i <= points2.Count; i++)
                {
                    points2Offset.Add(points2[(i + offset) % points2.Count]);
                }

                Score score = CalcLinearListScore(points1, points2Offset);

                if (score > maxScore || offset == 0)
                {
                    maxScore = score;
                }
            }

            return maxScore;
        }


        private Score CalcLinearListScore(List<Vector2> points1, List<Vector2> points2)
        {

            float posDist = CalcPositionDistance(points1, points2);
            float curvDist = CalcCurvatureDistance(points1, points2);
            float angleDist = CalcAngleDistance(points1, points2);

            return new Score() { positionDistance = posDist, curvatureDistance = curvDist, angleDistance = angleDist };
        }

        private float CalcPositionDistance(List<Vector2> points1, List<Vector2> points2)
        {
            float sqrt2 = Mathf.Sqrt(2);

            float sumDistance = 0;

            int n = points1.Count;
            for (int i = 0; i < n; i++)
            {
                float dif = Vector2.Distance(points1[i], points2[i]) / sqrt2;
                sumDistance += dif;
            }

            return sumDistance;
        }

        private float CalcCurvatureDistance(List<Vector2> points1, List<Vector2> points2)
        {

            int n = points1.Count;

            var curv1 = CalcCurvature(points1);
            var curv2 = CalcCurvature(points2);

            float sumCurvDistance = 0;

            for (int i = 0; i < n; i++)
            {
                float dif = Mathf.Abs(curv1[i] - curv2[i]) / 360f;
                sumCurvDistance += dif;
            }

            return sumCurvDistance;
        }

        private float CalcAngleDistance(List<Vector2> points1, List<Vector2> points2)
        {

            int n = points1.Count;

            var angles1 = CalcAngles(points1);
            var angles2 = CalcAngles(points2);

            float sumAngleDistance = 0;

            for (int i = 0; i < n; i++)
            {
                float dif = Mathf.Abs(Mathf.DeltaAngle(angles1[i], angles2[i])) / 360f;
                sumAngleDistance += dif;
            }

            return sumAngleDistance;
        }

        private List<float> CalcAngles(List<Vector2> points)
        {
            int step = 10;
            List<float> result = new List<float>();

            for (int i = 0; i < points.Count; i++)
            {
                int i1 = Mathf.Max(i - step, 0);
                int i2 = Mathf.Min(i + step, points.Count - 1);
                var v1 = points[i1];
                var v2 = points[i2];
                var dir = v2 - v1;
                var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
                if (angle < 0)
                    angle += 360;
                result.Add(angle);
            }
            return result;
        }

        private List<float> CalcCurvature(List<Vector2> points)
        {
            int step = 10;
            List<float> result = new List<float>();
            for (int i = 0; i < step; i++)
                result.Add(0);
            for (int i = step; i < points.Count - step; i++)
            {
                var p1 = points[i - step];
                var p2 = points[i];
                var p3 = points[i + step];
                var v1 = p2 - p1;
                var v2 = p3 - p2;
                var angle1 = Mathf.Atan2(v1.y, v1.x) * Mathf.Rad2Deg;
                var angle2 = Mathf.Atan2(v2.y, v2.x) * Mathf.Rad2Deg;
                var angle = Mathf.DeltaAngle(angle1, angle2);
                result.Add(angle);
            }
            for (int i = 0; i < step; i++)
                result.Add(0);
            return result;
        }
    }
}