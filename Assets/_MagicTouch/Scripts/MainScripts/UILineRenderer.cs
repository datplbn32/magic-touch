using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace _MagicTouch.Scripts.MainScripts
{
    public class UILineRenderer : MaskableGraphic
    {
        /// <summary>
        /// Loại đoạn
        /// </summary>
        private enum SegmentType
        {
            Start = 1,
            Middle = 2,
            End = 3,
        }

        /// <summary>
        /// Góc xiên đẹp nhỏ nhất (rad - 30 deg)<br></br><br></br>
        /// Một phép nối xiên đẹp để thay thế các đỉnh
        /// của đoạn đường thay vì chỉ hiển thị một hình tứ giác để kết nối các điểm cuối.
        /// Điều này cải thiện hình thức của các đường kết cấu và trong suốt vì không có sự chồng chéo.
        /// </summary>
        private const float MIN_BEVEL_NICE_JOIN = 30 * Mathf.Deg2Rad;


        private static readonly Vector2 UV_TOP_LEFT = Vector2.up;
        private static readonly Vector2 UV_BOTTOM_LEFT = Vector2.zero;
        private static readonly Vector2 UV_TOP_CENTER = new(0.5f, 1);
        private static readonly Vector2 UV_BOTTOM_CENTER = new(0.5f, 0);
        private static readonly Vector2 UV_TOP_RIGHT = new(1, 1);
        private static readonly Vector2 UV_BOTTOM_RIGHT = new(1, 0);


        private static readonly Vector2[] StartUvs = { UV_BOTTOM_LEFT, UV_TOP_LEFT, UV_TOP_CENTER, UV_BOTTOM_CENTER };
        private static readonly Vector2[] MiddleUvs = { UV_BOTTOM_CENTER, UV_TOP_CENTER, UV_TOP_CENTER, UV_BOTTOM_CENTER };
        private static readonly Vector2[] EndUvs = { UV_BOTTOM_CENTER, UV_TOP_CENTER, UV_TOP_RIGHT, UV_BOTTOM_RIGHT };


        [Tooltip("Texture để vẽ")]
        [SerializeField] Texture _textureToDraw;


        [Tooltip("Độ dày của line")]
        [SerializeField] private float _lineThickness = 2;


        private List<Vector2> _listPoints = new();


        public override Texture mainTexture => _textureToDraw == null ? s_WhiteTexture : _textureToDraw;


        public List<Vector2> ListPoints => _listPoints;


        protected override void OnPopulateMesh(VertexHelper vh)
        {
            Rect rect = rectTransform.rect;
            var sizeX = rect.width;
            var sizeY = rect.height;
            var pivot = rectTransform.pivot;
            var offsetX = -pivot.x * rect.width;
            var offsetY = -pivot.y * rect.height;

            vh.Clear();

            // Generate the quads that make up the wide line
            var segments = new List<UIVertex[]>();


            for (var i = 1; i < ListPoints.Count; i++)
            {
                var start = ListPoints[i - 1];
                var end = ListPoints[i];
                start = new Vector2(start.x * sizeX + offsetX, start.y * sizeY + offsetY);
                end = new Vector2(end.x * sizeX + offsetX, end.y * sizeY + offsetY);

                segments.Add(CreateLineSegment(start, end, SegmentType.Middle));
            }

            // Add the line segments to the vertex helper, creating any joins as needed
            for (var i = 0; i < segments.Count; i++)
            {
                if (i < segments.Count - 1)
                {
                    var vec1 = segments[i][1].position - segments[i][2].position;
                    var vec2 = segments[i + 1][2].position - segments[i + 1][1].position;
                    var angle = Vector2.Angle(vec1, vec2) * Mathf.Deg2Rad;

                    // Positive sign means the line is turning in a 'clockwise' direction
                    var sign = Mathf.Sign(Vector3.Cross(vec1.normalized, vec2.normalized).z);

                    // Calculate the miter point
                    var miterDistance = _lineThickness / (2 * Mathf.Tan(angle / 2));
                    var miterPointA = segments[i][2].position - vec1.normalized * miterDistance * sign;
                    var miterPointB = segments[i][3].position + vec1.normalized * miterDistance * sign;

                    if (miterDistance < vec1.magnitude / 2 && miterDistance < vec2.magnitude / 2 && angle > MIN_BEVEL_NICE_JOIN)
                    {
                        if (sign < 0)
                        {
                            segments[i][2].position = miterPointA;
                            segments[i + 1][1].position = miterPointA;
                        }
                        else
                        {
                            segments[i][3].position = miterPointB;
                            segments[i + 1][0].position = miterPointB;
                        }
                    }

                    var join = new[] { segments[i][2], segments[i][3], segments[i + 1][0], segments[i + 1][1] };
                    vh.AddUIVertexQuad(join);
                }
                vh.AddUIVertexQuad(segments[i]);
            }
        }

        private UIVertex[] CreateLineSegment(Vector2 start, Vector2 end, SegmentType type)
        {
            var uvs = MiddleUvs;
            if (type == SegmentType.Start)
                uvs = StartUvs;
            else if (type == SegmentType.End)
                uvs = EndUvs;

            Vector2 offset = new Vector2(start.y - end.y, end.x - start.x).normalized * _lineThickness / 2;
            var v1 = start - offset;
            var v2 = start + offset;
            var v3 = end + offset;
            var v4 = end - offset;
            return SetVbo(new[] { v1, v2, v3, v4 }, uvs);
        }


        private UIVertex[] SetVbo(Vector2[] vertices, Vector2[] uvs)
        {
            UIVertex[] vbo = new UIVertex[4];
            for (int i = 0; i < vertices.Length; i++)
            {
                var vert = UIVertex.simpleVert;
                vert.color = color;
                vert.position = vertices[i];
                vert.uv0 = uvs[i];
                vbo[i] = vert;
            }
            return vbo;
        }
    }
}