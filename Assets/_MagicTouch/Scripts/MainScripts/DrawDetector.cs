using System.Collections;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

namespace _MagicTouch.Scripts.MainScripts
{
    public class DrawDetector : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        private GestureLine _gestureLineData = new();
        [SerializeField] private UILineRenderer _lineRender;
        private RectTransform _rectTransform;
        [SerializeField] private Recognizer _recognizer;
        private float scoreToAccept;


        private void Start()
        {
            _rectTransform = transform as RectTransform;
        }


        /// <summary>
        /// Bắt đầu vẽ
        /// </summary>
        /// <param name="eventData"></param>
        public void OnBeginDrag(PointerEventData eventData)
        {
            Debug.LogError("VAR");

            // Tạo line mới
            _gestureLineData.Clear();

            Draw(eventData);
        }


        public void OnDrag(PointerEventData eventData)
        {
            Draw(eventData);
        }


        /// <summary>
        /// Vẽ điểm
        /// </summary>
        /// <param name="eventData"></param>
        private void Draw(PointerEventData eventData)
        {
            // Lấy bị trí tại điểm vẽ
            var fixedPos = FixedPosition(eventData.position);

            // Nếu là điểm mới => vẽ
            if (_gestureLineData.ListPoints.Count == 0 || _gestureLineData.ListPoints.Last() != fixedPos)
            {
                // Thêm vào data
                _gestureLineData.ListPoints.Add(fixedPos);

                // Update
                UpdateLines();
            }
        }


        public void OnEndDrag(PointerEventData eventData)
        {
            StartCoroutine(OnEndDragCoroutine(eventData));
        }


        private IEnumerator OnEndDragCoroutine(PointerEventData eventData)
        {
            _gestureLineData.ListPoints.Add(FixedPosition(eventData.position));
            UpdateLines();

            var sizedNormalizedData = _gestureLineData;

            RecognitionResult result = null;

            //run in another thread

            var thread = new System.Threading.Thread(() => { result = _recognizer.Recognize(sizedNormalizedData); });
            thread.Start();
            while (thread.IsAlive)
            {
                yield return null;
            }

            Debug.LogError(result.score.score);
            Debug.LogError(result.gesture.name);
        }


        private Vector2 FixedPosition(Vector2 pos)
        {
            return pos;
        }


        /// <summary>
        /// Thêm các điểm vào _lineRender (Có clamp ở trong vùng đó)
        /// </summary>
        private void UpdateLines()
        {
            _lineRender.ListPoints.Clear();
            _lineRender.SetAllDirty();
            _lineRender.ListPoints.AddRange(_gestureLineData.ListPoints.Select(RealToLine));
            _lineRender.SetAllDirty();
        }


        Vector2 RealToLine(Vector2 position)
        {
            var local = _rectTransform.InverseTransformPoint(position);
            var normalized = Rect.PointToNormalized(_rectTransform.rect, local);
            return normalized;
        }
    }
}