using UnityEngine;

namespace _MagicTouch.Scripts.MainScripts
{
    [CreateAssetMenu(fileName = "GesturePattern", menuName = "Magic Touch/GesturePattern", order = 0)]
    public class GesturePattern : ScriptableObject
    {
        [SerializeField] private string _id;
        [SerializeField] private GestureLine _gestureLine;

        
        public string ID => _id;
        public GestureLine GestureLine => _gestureLine;
    }
}