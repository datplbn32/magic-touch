using System.Collections.Generic;
using UnityEngine;

namespace _MagicTouch.Scripts.MainScripts
{
    [System.Serializable]
    public class GestureLine
    {
        [SerializeField] private List<Vector2> _listPoints = new();
        [SerializeField] private bool _closedLine;

        
        public bool ClosedLine
        {
            get => _closedLine;
            set => _closedLine = value;
        }

        public List<Vector2> ListPoints
        {
            get => _listPoints;
            set => _listPoints = value;
        }

        public void Clear()
        {
            _listPoints.Clear();
        }
    }
}